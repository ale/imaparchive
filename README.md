imaparchive
===========

Archives messages from a (remote) mailbox to a local one, mirroring
the remote folder tree.

## Installation

    $ go get git.autistici.org/ale/imaparchive

## Usage

The *imaparchive* command-line tool takes a source and a destination
mailbox, scans all subfolders, and copies all messages from source to
destination that are not already present. It can filter for messages
older than a certain age, and encrypt the destination messages with
PGP/MIME.

Mailboxes are specified with a URL-like syntax, and can be one of:

* IMAP accounts, with syntax

        imaps://USER:PASSWORD@SERVER:993/FOLDER

  where USER, PASSWORD, SERVER and (optionally) FOLDER must be set
  accordingly.
* local directories in Maildir format:

        maildir:PATH

  relative paths are evaluated from the current directory.

To encrypt messages on the destination with PGP/MIME, you should store
your armored public key in a file, and pass it to the tool with the
*--pgp-key* option:

    imaparchive --pgp-key=my_key.asc --src=... --dst=...

The tool can delete messages on the source, once it has successfully
copied them to the destination. To make it do so, use the *--delete*
option.

### Folder names

Both IMAP and Maildir sources recursively scan the structure of
subfolders of the specified source URL. The folder *name* is just the
part of the tree that is not included in the source URL. In practical
terms: when you specify a folder in an IMAP URL, it will be considered
part of the "root" and won't be reproduced on the destination folder
names.

For instance, with a source URL like:

    imaps://user:password@server:993/archive/2014

And a destination of

    maildir:local/

A folder named *archive/2014/July* on the source will result in the
creation of a local directory named *local/July*.
