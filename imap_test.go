package main

import (
	"bytes"
	"fmt"
	"math/rand"
	"net"
	"strings"
	"testing"
	"time"

	imap "github.com/emersion/go-imap"
	"github.com/emersion/go-imap/backend"
	imem "github.com/emersion/go-imap/backend/memory"
	"github.com/emersion/go-imap/server"
)

var (
	testUsername = "username"
	testPassword = "password"
)

type imapTestMessage struct {
	//flags []string
	body []byte
}

func (m imapTestMessage) Literal() imap.Literal {
	return bytes.NewBuffer(m.body)
}

type testMailboxData map[string][]*imapTestMessage

var randomWords = []string{
	"flashlight", "japan", "bonobo", "ternary", "sheep", "boat", "kangaroo",
	"stamp", "spot", "porridge", "historical", "people", "world", "banana",
}

func randomWord() string {
	return randomWords[rand.Intn(len(randomWords))]
}

func randomPhrase(n int) string {
	var words []string
	for i := 0; i < n; i++ {
		words = append(words, randomWord())
	}
	words[0] = strings.Title(words[0])
	return strings.Join(words, " ") + "."
}

func randomAddr() string {
	return fmt.Sprintf("%s@%s-%s.com", randomWord(), randomWord(), randomWord())
}

var curMessageID int

func newTestMessage(recent bool) *imapTestMessage {
	curMessageID++
	messageID := fmt.Sprintf("<%09d@test.local>", curMessageID)
	date := time.Date(2006+rand.Intn(10), time.Month(1+rand.Intn(11)), 1+rand.Intn(20), 10, 12, 13, 0, time.UTC)
	if recent {
		date = time.Now().UTC()
	}
	hdr := fmt.Sprintf(
		`From: %s
To: %s
Subject: %s
Date: %s
Message-ID: %s
Content-Type: text/plain
`,
		randomAddr(),
		randomAddr(),
		randomPhrase(4),
		date.Format(time.RFC1123Z),
		messageID,
	)
	hdr = strings.Replace(hdr, "\n", "\r\n", -1)
	body := hdr + "\r\n" + randomPhrase(64)
	return &imapTestMessage{body: []byte(body)}
}

func newTestBackend(data testMailboxData) backend.Backend {
	// The go-imap backend/memory test object comes with pre-set
	// username and password, and an INBOX with a single test
	// message in it.
	be := imem.New()
	loadTestData(be, data)
	return be
}

func loadTestData(be backend.Backend, data testMailboxData) {
	user, _ := be.Login(testUsername, testPassword)
	defer user.Logout()
	inbox, _ := user.GetMailbox("INBOX")

	// Delete the existing message.
	set := new(imap.SeqSet)
	set.AddRange(1, 1)
	inbox.UpdateMessagesFlags(false, set, imap.AddFlags, []string{imap.DeletedFlag})
	inbox.Expunge()

	for folder, messages := range data {
		realFolder := "INBOX/" + folder
		user.CreateMailbox(realFolder)
		mbox, _ := user.GetMailbox(realFolder)
		for _, msg := range messages {
			mbox.CreateMessage([]string{imap.SeenFlag}, time.Now(), msg.Literal())
		}
	}
}

type testIMAPServer struct {
	l   net.Listener
	s   *server.Server
	url string
}

func newTestServer(t testing.TB, data testMailboxData) *testIMAPServer {
	be := newTestBackend(data)

	l, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		t.Fatal("Cannot listen:", err)
	}

	s := server.New(be)
	s.AllowInsecureAuth = true
	go s.Serve(l)

	return &testIMAPServer{
		l:   l,
		s:   s,
		url: fmt.Sprintf("imap://%s:%s@%s/", testUsername, testPassword, l.Addr().String()),
	}
}

func (i *testIMAPServer) Close() {
	i.s.Close()
	i.l.Close()
}

var defaultTestMailboxData = testMailboxData{
	"folder1": []*imapTestMessage{
		newTestMessage(false),
	},
	"folder2": []*imapTestMessage{
		newTestMessage(false),
		newTestMessage(true),
	},
}

func runTestSourceListFolders(t *testing.T, srcurl string) {
	src, err := newSource(srcurl)
	if err != nil {
		t.Fatalf("newSource(%s): %v", srcurl, err)
	}
	defer src.Close()
	folders, err := src.ListFolders()
	if err != nil {
		t.Fatalf("ListFolders(%s): %v", srcurl, err)
	}
	if len(folders) != 3 {
		t.Fatalf("ListFolders(%s) returned %d folders, expected 3", srcurl, len(folders))
	}
}

func TestIMAPSource_ListFolders(t *testing.T) {
	s := newTestServer(t, defaultTestMailboxData)
	defer s.Close()

	runTestSourceListFolders(t, s.url)
}

func runTestSourceListMessages(t *testing.T, srcurl string) {
	src, err := newSource(srcurl)
	if err != nil {
		t.Fatalf("newSource(%s): %v", srcurl, err)
	}
	defer src.Close()
	messages, err := src.ListMessages("folder2")
	if err != nil {
		t.Fatalf("ListMessages(%s): %v", srcurl, err)
	}
	if len(messages) != 2 {
		t.Fatalf("ListMessages(%s) returned %d messages, expected 2", srcurl, len(messages))
	}
}

func TestIMAPSource_ListMessages(t *testing.T) {
	s := newTestServer(t, defaultTestMailboxData)
	defer s.Close()

	runTestSourceListMessages(t, s.url)
}
