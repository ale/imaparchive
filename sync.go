package main

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"log"
	"net/mail"
	"regexp"
	"strings"
	"sync"
	"time"

	"github.com/vbauerster/mpb"
	"github.com/vbauerster/mpb/decor"
)

// A source of messages.
type mailboxSource interface {
	ListFolders() ([]string, error)
	ListMessages(string) ([]message, error)
	FetchMessages(string, []message) <-chan *mail.Message
	DeleteMessages(string, []message) error
	Close()
}

// A destination for messages.
type mailboxDestination interface {
	mailboxSource
	CreateFolder(string) error
	WriteMessage(string, *mail.Message) error
}

// An opaque message type (that is passed between ListMessages and
// FetchMessages).
type message interface {
	Date() time.Time
	MessageID() string
}

func newSource(u string) (mailboxSource, error) {
	switch {
	case strings.HasPrefix(u, "imaps://"), strings.HasPrefix(u, "imap://"):
		return newIMAPSource(u)
	case strings.HasPrefix(u, "maildir:"):
		return &maildirSource{root: u[8:]}, nil
	default:
		return nil, errors.New("unsupported URL scheme")
	}
}

func newDestination(u, pgpKeyFile string) (mailboxDestination, error) {
	var d mailboxDestination
	var err error
	switch {
	case strings.HasPrefix(u, "imaps://"):
		d, err = newIMAPDest(u)
	case strings.HasPrefix(u, "maildir:"):
		d, err = newMaildirDest(u[8:]), nil
	default:
		return nil, errors.New("unsupported URL scheme")
	}
	if err != nil {
		return nil, err
	}
	if pgpKeyFile != "" {
		d, err = newGPGFilter(d, pgpKeyFile)
		if err != nil {
			return nil, err
		}
	}
	return d, nil
}

type regexpList []*regexp.Regexp

func newRegexpList(patterns []string) regexpList {
	var l regexpList
	for _, s := range patterns {
		p := regexp.MustCompile("^" + s + "$")
		l = append(l, p)
	}
	return l
}

func (l regexpList) Add(s string) regexpList {
	return append(l, regexp.MustCompile("^"+s+"$"))
}

func (l regexpList) Match(s string) bool {
	for _, p := range l {
		if p.MatchString(s) {
			return true
		}
	}
	return false
}

func parseMessageID(s string) string {
	if s != "" && s[0] == '<' {
		return s[1 : len(s)-2]
	}
	return s
}

type messageFilterFunc func(message) bool

func messagesInFolder(src mailboxSource, folder string, selectionFilter messageFilterFunc) (map[string]message, error) {
	set := make(map[string]message)
	msgs, err := src.ListMessages(folder)
	if err != nil {
		return nil, err
	}
	for _, m := range msgs {
		if selectionFilter == nil || selectionFilter(m) {
			set[m.MessageID()] = m
		}
	}
	return set, nil
}

func writeMessage(msg *mail.Message, w io.Writer) error {
	for hdr, values := range msg.Header {
		for _, value := range values {
			fmt.Fprintf(w, "%s: %s\r\n", hdr, value) // nolint
		}
	}
	fmt.Fprintf(w, "\r\n") // nolint
	_, err := io.Copy(w, msg.Body)
	return err
}

func messageToBuffer(msg *mail.Message) (*bytes.Buffer, error) {
	var buf bytes.Buffer
	if err := writeMessage(msg, &buf); err != nil {
		return nil, err
	}
	return &buf, nil
}

func olderThan(days int) messageFilterFunc {
	t := time.Now().AddDate(0, 0, -days)
	return func(m message) bool {
		return m.Date().Before(t)
	}
}

func copyMessages(src mailboxSource, dst mailboxDestination, folder string, messages []message, progress *mpb.Progress) (int, error) {
	var bar *mpb.Bar
	if progress != nil {
		bar = progress.AddBar(
			int64(len(messages)),
			mpb.PrependDecorators(decor.StaticName(folder, 20, 0)),
			mpb.AppendDecorators(decor.DynamicName(func(s *decor.Statistics) string {
				return fmt.Sprintf("%d / %d", s.Current, s.Total)
			}, 0, decor.DwidthSync)),
		)
		defer bar.Complete()
		defer progress.RemoveBar(bar)
	}

	count := 0
	for m := range src.FetchMessages(folder, messages) {
		if progress != nil {
			bar.Increment()
		}
		if err := dst.WriteMessage(folder, m); err != nil {
			return count, err
		}
		count++
	}
	return count, nil
}

func syncFolder(src mailboxSource, dst mailboxDestination, folder string, selectionFilter messageFilterFunc, progress *mpb.Progress, deleteSourceMessages bool) error {
	// Build set of Message-IDs on source and destination in
	// parallel, then compute set difference.
	var srcMsgs, dstMsgs map[string]message
	var err error
	var wg sync.WaitGroup
	wg.Add(2)
	go func() {
		srcMsgs, err = messagesInFolder(src, folder, selectionFilter)
		wg.Done()
	}()
	go func() {
		// Ignore errors on destination, it's ok if the folder
		// does not exist for example.
		dstMsgs, _ = messagesInFolder(dst, folder, nil) // nolint
		wg.Done()
	}()
	wg.Wait()

	if err != nil {
		return err
	}

	// Keep track of the *source* messages that should be deleted,
	// if we're asked to do so.
	var toDelete []message
	for _, m := range dstMsgs {
		if srcMsg, ok := srcMsgs[m.MessageID()]; ok {
			delete(srcMsgs, m.MessageID())
			if deleteSourceMessages {
				toDelete = append(toDelete, srcMsg)
			}
		}
	}

	// Copy what's left.
	var toCopy []message
	for _, m := range srcMsgs {
		toCopy = append(toCopy, m)
	}
	if len(toCopy) == 0 {
		return nil
	}
	if err = dst.CreateFolder(folder); err != nil {
		log.Printf("warning: create folder %s: %v", folder, err)
	}

	if _, err = copyMessages(src, dst, folder, toCopy, progress); err != nil {
		return err
	}

	// Delete messages that were successfully copied. This is done
	// only at the end of a successful sync, rather than
	// incrementally, for performance reasons mostly - it's
	// slightly safer but could be surprising behavior on failure.
	if len(toDelete) > 0 {
		return src.DeleteMessages(folder, toDelete)
	}

	return nil
}

func (op *syncOp) worker(folderCh <-chan string, progress *mpb.Progress) {
	src, err := newSource(op.srcurl)
	if err != nil {
		log.Printf("source connection error: %v", err)
		return
	}
	defer src.Close()

	dst, err := newDestination(op.dsturl, op.pgpKeyFile)
	if err != nil {
		log.Printf("target connection error: %v", err)
		return
	}
	defer dst.Close()

	for folder := range folderCh {
		if err := syncFolder(src, dst, folder, op.selectionFilter, progress, op.deleteSourceMessages); err != nil {
			log.Printf("error processing mailbox %s: %v", folder, err)
		}
	}
}

var defaultExcludePatterns = []string{"Trash", "[sS]pam", "Junk", "Drafts"}

func listSourceFolders(srcurl string, excludeFolders regexpList) ([]string, error) {
	src, err := newSource(srcurl)
	if err != nil {
		return nil, err
	}
	defer src.Close()

	folders, err := src.ListFolders()
	if err != nil {
		return nil, err
	}
	var out []string
	for _, f := range folders {
		if !excludeFolders.Match(f) {
			out = append(out, f)
		}
	}
	return out, nil
}

type syncOp struct {
	srcurl, dsturl       string
	pgpKeyFile           string
	concurrency          int
	selectionFilter      messageFilterFunc
	deleteSourceMessages bool
	excludeFolders       regexpList
}

func newSyncOp(srcurl, dsturl string) *syncOp {
	return &syncOp{
		srcurl:         srcurl,
		dsturl:         dsturl,
		excludeFolders: newRegexpList(defaultExcludePatterns),
	}
}

func (op *syncOp) run() error {
	folders, err := listSourceFolders(op.srcurl, op.excludeFolders)
	if err != nil {
		log.Printf("source connection error: %v", err)
		return err
	}

	var progress *mpb.Progress
	if !*quiet {
		progress = mpb.New()
		defer progress.Stop()
	}

	// Spawn the work off to a number of workers, each handling a
	// single folder at a time. It's the strategy that works best
	// for IMAP, while it doesn't really matter for local mail
	// storage. Each worker has its own mailboxSource instance.
	var wg sync.WaitGroup
	folderCh := make(chan string, op.concurrency)
	for i := 0; i < op.concurrency; i++ {
		wg.Add(1)
		go func() {
			op.worker(folderCh, progress)
			wg.Done()
		}()
	}

	for _, f := range folders {
		folderCh <- f
	}

	close(folderCh)
	wg.Wait()

	return nil
}
