package main

import (
	"errors"
	"log"
	"net/mail"
	"net/url"
	"strings"
	"time"

	imap "github.com/emersion/go-imap"
	"github.com/emersion/go-imap/client"
)

var (
	imapFetchBatchSize  = 50
	imapDeleteBatchSize = 1000

	bodyAndHeadersSectionName *imap.BodySectionName
	bodySectionName           *imap.BodySectionName
)

func init() {
	var err error
	bodyAndHeadersSectionName, err = imap.ParseBodySectionName(
		imap.FetchItem("BODY[HEADER.FIELDS (MESSAGE-ID)]"))
	if err != nil {
		log.Fatal(err)
	}
	bodySectionName, err = imap.ParseBodySectionName(
		imap.FetchItem("BODY[]"))
	if err != nil {
		log.Fatal(err)
	}
}

type connectionParams struct {
	ssl        bool
	user       string
	password   string
	server     string
	mboxPrefix string
}

func parseImapURL(s string) (*connectionParams, error) {
	u, err := url.Parse(s)
	if err != nil {
		return nil, err
	}
	ssl := false
	switch u.Scheme {
	case "imap":
	case "imaps":
		ssl = true
	default:
		return nil, errors.New("unsupported URL scheme")
	}
	if u.User == nil {
		return nil, errors.New("no user information in URL")
	}
	pw, ok := u.User.Password()
	if !ok {
		return nil, errors.New("no password specified")
	}
	pfx := strings.TrimSuffix(u.Path, "/")
	if pfx == "" {
		pfx = "INBOX"
	}
	return &connectionParams{
		ssl:        ssl,
		user:       u.User.Username(),
		password:   pw,
		server:     u.Host,
		mboxPrefix: pfx,
	}, nil
}

func hasPrefix(s, prefix string) bool {
	// Match IMAP mbox names.
	return s == prefix || strings.HasPrefix(s, prefix+"/")
}

func stripPrefix(s, prefix string) string {
	return strings.TrimPrefix(s, prefix+"/")
}

func addPrefix(s, prefix string) string {
	if s == "" {
		return prefix
	}
	return prefix + "/" + s
}

type imapMessage struct {
	msg       *imap.Message
	messageID string
}

func (m *imapMessage) Date() time.Time {
	return m.msg.InternalDate
}

func (m *imapMessage) MessageID() string {
	return m.messageID
}

type imapSource struct {
	params *connectionParams
	client *client.Client
}

func connectIMAP(params *connectionParams) (*client.Client, error) {
	var c *client.Client
	var err error
	if params.ssl {
		c, err = client.DialTLS(params.server, nil)
	} else {
		c, err = client.Dial(params.server)
	}
	if err != nil {
		return nil, err
	}
	c.ErrorLog = &nilLogger{}
	if err = c.Login(params.user, params.password); err != nil {
		// Ignore errors but try to close the connection.
		c.Logout() // nolint
		return nil, err
	}
	return c, nil
}

func newIMAPSource(u string) (*imapSource, error) {
	params, err := parseImapURL(u)
	if err != nil {
		return nil, err
	}
	client, err := connectIMAP(params)
	if err != nil {
		return nil, err
	}
	return &imapSource{
		params: params,
		client: client,
	}, nil
}

func (s *imapSource) folderName(folder string) string {
	return addPrefix(folder, s.params.mboxPrefix)
}

func (s *imapSource) Close() {
	s.client.Logout() // nolint
}

func (s *imapSource) ListFolders() ([]string, error) {
	mailboxCh := make(chan *imap.MailboxInfo, 10)
	done := make(chan error, 1)
	go func() {
		done <- s.client.List("", "*", mailboxCh)
	}()

	var mboxes []string
	for m := range mailboxCh {
		if !hasPrefix(m.Name, s.params.mboxPrefix) {
			continue
		}
		relName := stripPrefix(m.Name, s.params.mboxPrefix)
		mboxes = append(mboxes, relName)
	}

	return mboxes, <-done
}

func isDeleted(msg *imap.Message) bool {
	for _, f := range msg.Flags {
		if f == imap.DeletedFlag {
			return true
		}
	}
	return false
}

func (s *imapSource) ListMessages(folder string) ([]message, error) {
	mbox, err := s.client.Select(s.folderName(folder), true)
	if err != nil {
		log.Printf("Select error for %s: %v", folder, err)
		return nil, err
	}
	if mbox.Messages == 0 {
		return nil, nil
	}

	// Fetch attributes for all messages so we can check the
	// destination. We don't know the message UIDs yet, so just
	// request a sequence number range (from 1 to the total number
	// of messages).
	seqset := new(imap.SeqSet)
	seqset.AddRange(1, mbox.Messages)
	attrs := []imap.FetchItem{
		imap.FetchUid,
		imap.FetchInternalDate,
		imap.FetchFlags,
		"BODY.PEEK[HEADER.FIELDS (MESSAGE-ID)]",
	}

	messages := make(chan *imap.Message, 100)
	done := make(chan error, 1)
	go func() {
		done <- s.client.Fetch(seqset, attrs, messages)
	}()

	var out []message
	for msg := range messages {
		if isDeleted(msg) {
			continue
		}
		body := msg.GetBody(bodyAndHeadersSectionName)
		if body == nil {
			continue
		}
		parsed, err := mail.ReadMessage(body)
		if err != nil {
			log.Printf("ReadMessage error: %v", err)
			continue
		}
		messageID := parseMessageID(parsed.Header.Get("Message-Id"))
		if messageID == "" {
			//log.Printf("message without ID: %v", parsed.Header)
			continue
		}
		out = append(out, &imapMessage{
			msg:       msg,
			messageID: messageID,
		})
	}

	return out, <-done
}

func (s *imapSource) fetchBatch(batch []message, ch chan *mail.Message) {
	set := new(imap.SeqSet)
	for _, msg := range batch {
		set.AddNum(msg.(*imapMessage).msg.Uid)
	}
	attrs := []imap.FetchItem{"BODY.PEEK[]"}
	messages := make(chan *imap.Message, 100)
	done := make(chan error, 1)
	go func() {
		done <- s.client.UidFetch(set, attrs, messages)
	}()

	for msg := range messages {
		body := msg.GetBody(bodySectionName)
		if body == nil {
			continue
		}
		parsed, err := mail.ReadMessage(body)
		if err != nil {
			log.Printf("ReadMessage error: %v", err)
			continue
		}
		ch <- parsed
	}

	if err := <-done; err != nil {
		log.Printf("imap Fetch error: %v", err)
	}
}

func (s *imapSource) FetchMessages(folder string, msgs []message) <-chan *mail.Message {
	_, err := s.client.Select(s.folderName(folder), true)
	if err != nil {
		log.Printf("Select error for %s: %v", folder, err)
		return nil
	}

	batchSize := imapFetchBatchSize
	ch := make(chan *mail.Message, batchSize*2)
	go func() {
		defer close(ch)

		for i := 0; i < len(msgs); i += batchSize {
			j := i + batchSize
			if j > len(msgs) {
				j = len(msgs)
			}
			batch := msgs[i:j]
			s.fetchBatch(batch, ch)
		}
	}()
	return ch
}

func (s *imapSource) deleteBatch(msgs []message) error {
	set := new(imap.SeqSet)
	for _, msg := range msgs {
		set.AddNum(msg.(*imapMessage).msg.Uid)
	}
	return s.client.UidStore(set, imap.FormatFlagsOp(imap.AddFlags, true), []string{imap.DeletedFlag}, nil)
}

func (s *imapSource) DeleteMessages(folder string, msgs []message) error {
	// Expects the folder to be already selected!
	batchSize := imapDeleteBatchSize
	for i := 0; i < len(msgs); i += batchSize {
		j := i + batchSize
		if j > len(msgs) {
			j = len(msgs)
		}
		if err := s.deleteBatch(msgs[i:j]); err != nil {
			return err
		}
	}
	return s.client.Expunge(nil)
}

type imapDest struct {
	*imapSource
}

func newIMAPDest(u string) (*imapDest, error) {
	src, err := newIMAPSource(u)
	if err != nil {
		return nil, err
	}
	return &imapDest{src}, nil
}

func (d *imapDest) CreateFolder(folder string) error {
	return d.client.Create(d.folderName(folder))
}

func (d *imapDest) WriteMessage(folder string, msg *mail.Message) error {
	buf, err := messageToBuffer(msg)
	if err != nil {
		return err
	}
	return d.client.Append(d.folderName(folder), []string{}, time.Now(), buf)
}

type nilLogger struct{}

func (l nilLogger) Printf(format string, v ...interface{}) {}
func (l nilLogger) Println(v ...interface{})               {}
