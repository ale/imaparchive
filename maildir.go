package main

import (
	"bufio"
	"bytes"
	"crypto/rand"
	"encoding/hex"
	"io"
	"net/mail"
	"net/textproto"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync/atomic"
	"time"
)

// Maildir type, inspired by github.com/luksen/maildir but without the
// accidentally quadratic behavior.
type Maildir string

// Create a maildir if it does not already exist.
func (d Maildir) Create() error {
	for _, sub := range []string{"cur", "new", "tmp"} {
		p := filepath.Join(string(d), sub)
		if _, err := os.Stat(p); os.IsNotExist(err) {
			if err := os.MkdirAll(p, 0700); err != nil {
				return err
			}
		}
	}
	return nil
}

// Unseen moves messages from the 'new' directory to 'cur', and
// returns their list.
func (d Maildir) Unseen() ([]string, error) {
	f, err := os.Open(filepath.Join(string(d), "new"))
	if err != nil {
		return nil, err
	}
	defer f.Close() // nolint
	names, err := f.Readdirnames(0)
	if err != nil {
		return nil, err
	}
	var keys []string
	for _, n := range names {
		if n[0] == '.' {
			continue
		}
		if err := os.Rename(filepath.Join(string(d), "new", n), filepath.Join(string(d), "cur", n)); err == nil {
			keys = append(keys, n)
		}
	}
	return keys, nil
}

// Keys returns keys for all messages in a folder.
func (d Maildir) Keys() ([]string, error) {
	f, err := os.Open(filepath.Join(string(d), "cur"))
	if err != nil {
		return nil, err
	}
	defer f.Close() // nolint
	names, err := f.Readdirnames(0)
	return names, err
}

// Header returns a parsed message header.
func (d Maildir) Header(key string) (mail.Header, error) {
	filename := filepath.Join(string(d), "cur", key)
	file, err := os.Open(filename) // #nosec
	if err != nil {
		return nil, err
	}
	defer file.Close() // nolint
	tp := textproto.NewReader(bufio.NewReader(file))
	hdr, err := tp.ReadMIMEHeader()
	if err != nil {
		return nil, err
	}
	return mail.Header(hdr), nil
}

// Message returns a Message by key.
func (d Maildir) Message(key string) (*mail.Message, error) {
	filename := filepath.Join(string(d), "cur", key)
	r, err := os.Open(filename) // #nosec
	if err != nil {
		return nil, err
	}
	defer r.Close() // nolint
	buf := new(bytes.Buffer)
	_, err = io.Copy(buf, r)
	if err != nil {
		return nil, err
	}
	return mail.ReadMessage(buf)
}

// Purge removes a message from the maildir.
func (d Maildir) Purge(key string) error {
	return os.Remove(filepath.Join(string(d), "cur", key))
}

// Delivery tracks the delivery of a single message to a maildir.
type Delivery struct {
	curPath, dstPath string
	f                *os.File
}

// Close the delivery successfully.
func (d *Delivery) Close() error {
	if err := d.f.Close(); err != nil {
		return err
	}
	return os.Rename(d.curPath, d.dstPath)
}

// Abort the delivery (remove temporary file).
func (d *Delivery) Abort() {
	d.f.Close()          // nolint
	os.Remove(d.curPath) // nolint
}

// Write the message body.
func (d *Delivery) Write(b []byte) (int, error) {
	return d.f.Write(b)
}

// NewDelivery starts writing a new message to the maildir.
func (d Maildir) NewDelivery() (*Delivery, error) {
	key, err := maildirKey()
	if err != nil {
		return nil, err
	}

	curPath := filepath.Join(string(d), "tmp", key)
	dstPath := filepath.Join(string(d), "new", key)
	f, err := os.Create(curPath)
	if err != nil {
		return nil, err
	}
	return &Delivery{
		f:       f,
		curPath: curPath,
		dstPath: dstPath,
	}, nil
}

var id int64

// maildirKey generates a new unique key as described in the Maildir
// specification.  For the third part of the key (delivery identifier)
// it uses an internal counter, the process id and a cryptographical
// random number to ensure uniqueness among messages delivered in the
// same second.
func maildirKey() (string, error) {
	var key string
	key += strconv.FormatInt(time.Now().Unix(), 10)
	key += "."
	host, err := os.Hostname()
	if err != nil {
		return "", err
	}
	host = strings.Replace(host, "/", "\057", -1)
	host = strings.Replace(host, ":", "\072", -1)
	key += host
	key += "."
	key += strconv.FormatInt(int64(os.Getpid()), 10)
	key += strconv.FormatInt(id, 10)
	atomic.AddInt64(&id, 1)
	bs := make([]byte, 10)
	_, err = io.ReadFull(rand.Reader, bs)
	if err != nil {
		return "", err
	}
	key += hex.EncodeToString(bs)
	return key, nil
}

type maildirMessage struct {
	key string
	hdr mail.Header
}

func (m *maildirMessage) Date() time.Time {
	// Return the zero value on error.
	t, _ := mail.ParseDate(m.hdr.Get("Date")) // nolint
	return t
}

func (m *maildirMessage) MessageID() string {
	return parseMessageID(m.hdr.Get("Message-ID"))
}

type maildirSource struct {
	root string
}

func (s *maildirSource) Close() {}

func isMaildir(path string) bool {
	stat, err := os.Stat(filepath.Join(path, "cur"))
	return err == nil && stat.IsDir()
}

func (s *maildirSource) ListFolders() ([]string, error) {
	var mboxes []string
	root := strings.TrimSuffix(s.root, "/")
	err := filepath.Walk(
		root,
		func(path string, fi os.FileInfo, err error) error {
			if err != nil {
				return nil
			}
			var relPath string
			if path != root {
				relPath = strings.TrimPrefix(path, root+"/")
			}
			if isMaildir(path) {
				mboxes = append(mboxes, relPath)
			}
			return nil
		},
	)
	return mboxes, err
}

func (s *maildirSource) ListMessages(folder string) ([]message, error) {
	dir := Maildir(filepath.Join(s.root, folder))

	var msgs []message
	keys, err := dir.Keys()
	if err != nil {
		return nil, err
	}
	unseen, _ := dir.Unseen() // nolint
	if len(unseen) > 0 {
		keys = append(keys, unseen...)
	}

	for _, key := range keys {
		hdr, err := dir.Header(key)
		if err != nil {
			continue
		}
		msgs = append(msgs, &maildirMessage{key: key, hdr: hdr})
	}
	return msgs, nil
}

func (s *maildirSource) FetchMessages(folder string, msgs []message) <-chan *mail.Message {
	dir := Maildir(filepath.Join(s.root, folder))

	ch := make(chan *mail.Message, 100)
	go func() {
		defer close(ch)
		for _, m := range msgs {
			msg, err := dir.Message(m.(*maildirMessage).key)
			if err == nil {
				ch <- msg
			}
		}
	}()
	return ch
}

func (s *maildirSource) DeleteMessages(folder string, msgs []message) error {
	dir := Maildir(filepath.Join(s.root, folder))
	for _, msg := range msgs {
		if err := dir.Purge(msg.(*maildirMessage).key); err != nil {
			return err
		}
	}
	return nil
}

type maildirDest struct {
	*maildirSource
}

func newMaildirDest(root string) *maildirDest {
	return &maildirDest{
		&maildirSource{
			root: root,
		},
	}
}

func (d *maildirDest) CreateFolder(folder string) error {
	dir := Maildir(filepath.Join(d.root, folder))
	return dir.Create()
}

func (d *maildirDest) WriteMessage(folder string, msg *mail.Message) error {
	dir := Maildir(filepath.Join(d.root, folder))
	delivery, err := dir.NewDelivery()
	if err != nil {
		return err
	}
	if err := writeMessage(msg, delivery); err != nil {
		delivery.Abort()
		return err
	}
	return delivery.Close()
}
