package main

import (
	"flag"
	"log"
	"strings"
)

var (
	srcURL        = flag.String("src", "", "source URL")
	dstURL        = flag.String("dst", "", "destination URL")
	excludes      = flag.String("exclude", "", "folders to exclude (comma-sep regexp list)")
	olderThanDays = flag.Int("older-than", 0, "only archive messages older than N days")
	quiet         = flag.Bool("quiet", false, "do not show progress bars")
	concurrency   = flag.Int("concurrency", 5, "number of concurrent source connections")
	doDelete      = flag.Bool("delete", false, "delete messages on source that were successfully copied")
	destPGPKey    = flag.String("pgp-key", "", "encrypt messages on destination with this key")
)

func main() {
	log.SetFlags(0)
	flag.Parse()

	op := newSyncOp(*srcURL, *dstURL)
	op.concurrency = *concurrency
	op.pgpKeyFile = *destPGPKey
	if *olderThanDays > 0 {
		op.selectionFilter = olderThan(*olderThanDays)
	}
	if *doDelete {
		op.deleteSourceMessages = true
	}
	if *excludes != "" {
		for _, pat := range strings.Split(*excludes, ",") {
			op.excludeFolders.Add(pat)
		}
	}

	if err := op.run(); err != nil {
		log.Fatal(err)
	}
}
