package main

import (
	"bytes"
	"io"
	"net/mail"
	"net/textproto"
	"os"
	"strings"

	pgpmime "github.com/emersion/go-pgpmime"
	"golang.org/x/crypto/openpgp"
)

type gpgFilter struct {
	mailboxDestination

	keys []*openpgp.Entity
}

func newGPGFilter(d mailboxDestination, keyFile string) (*gpgFilter, error) {
	f, err := os.Open(keyFile) // #nosec
	if err != nil {
		return nil, err
	}
	defer f.Close() // nolint
	keys, err := openpgp.ReadArmoredKeyRing(f)
	if err != nil {
		return nil, err
	}
	return &gpgFilter{
		mailboxDestination: d,
		keys:               keys,
	}, nil
}

func isEncrypted(msg *mail.Message) bool {
	return strings.HasPrefix(msg.Header.Get("Content-Type"), "multipart/encrypted")
}

func (f *gpgFilter) WriteMessage(folder string, msg *mail.Message) error {
	if isEncrypted(msg) {
		return f.mailboxDestination.WriteMessage(folder, msg)
	}

	// Create a new message header, preserving only some specific
	// headers.
	mh := make(mail.Header)
	for hdr, values := range msg.Header {
		switch hdr {
		case "From", "To", "Subject", "Message-Id", "Date", "In-Reply-To":
			mh[hdr] = values
		}
	}

	// Wrap and encrypt the original message with PGP/MIME. Setup
	// the writer first.
	var ciphertext bytes.Buffer
	cleartext := pgpmime.Encrypt(&ciphertext, textproto.MIMEHeader(msg.Header), f.keys, nil, nil)
	mh["Content-Type"] = []string{cleartext.ContentType()}
	mh["Mime-Version"] = []string{"1.0"}

	// Dump the original message body. Be careful with error
	// handling because we definitely don't want to archive a
	// corrupted PGP message.
	_, err := io.Copy(cleartext, msg.Body)
	if err != nil {
		cleartext.Close() // nolint
		return err
	}
	if err := cleartext.Close(); err != nil {
		return err
	}

	return f.mailboxDestination.WriteMessage(folder, &mail.Message{
		Header: mh,
		Body:   &ciphertext,
	})
}
